khr-fdw
=============

PostgreSQL foregin data wrapper built using [Multicorn](https://multicorn.org/) to fetch khr statistics data from NLS Finland: https://khr.maanmittauslaitos.fi/.
Take a look at the [API documentation](http://xml.nls.fi/Kiinteistonluovutukset/tietopalvelu/KiinteistokauppojenTilastopalvelu/Asiakasdokumentaatio/API.pdf) 
to find out more about the API.

Inspired greatly by [geofdw](https://github.com/bosth/geofdw).

# Installation

### Dependencies
```shell script
# Assuming you are running with root permissions (inside container) and have postgresql and postgis installed 
apt update
apt install python3-pip libpq-dev postgresql-server-dev-all postgresql-common
```

Install [Multicorn](https://multicorn.org/)
```shell script
git clone https://github.com/Kozea/Multicorn.git
cd Multicorn
make && make install
```
Install [plpygis](https://github.com/bosth/plpygis)
```shell script
git clone https://github.com/bosth/plpygis.git
cd plpygis
python3 setup.py install
```

Connect to your database and enable Multicorn
```sql
CREATE EXTENSION postgis;
CREATE EXTENSION multicorn;
```

### khr-fdw

Go to the to the khr-fdw directory and run
```shell script
python3 setup.py install
```

You have to restart Postgresql in order to find the package (`systemctl postgresql restart` or `service postgresql restart`).

# Usage

### Creating a fdw server
```sql
CREATE SERVER khr_fdw FOREIGN DATA WRAPPER multicorn OPTIONS ( wrapper 'khrfdw.Khr' );
```

### Creating fdw tables
Create following fdw tables
```sql
CREATE FOREIGN TABLE indicators (
    id INTEGER,
    title_fi varchar,
	title_en varchar
) server khr_fdw options(
	url 'indicators'
);

CREATE FOREIGN TABLE khrdata (
	indicator Integer,
    region Integer,
    year Smallint,
	value Numeric,
	category varchar
) server khr_fdw options(
	url 'data'
);

CREATE FOREIGN TABLE regions (
	id Integer,
	geom geometry(MULTIPOLYGON, 3067),
	code varchar,
	category varchar
) server khr_fdw options (
	url 'regions',
	srid '3067'
);
```

For regions it is recommended to create materialized views since the queries take quite long time. 
The possible values for categories are KUNTA (municipality), MAAKUNTA (region), SEUTUKUNTA (subregion), HILA5X5 (grid), 
HILA10X10 and HILA30X30.

```sql
CREATE MATERIALIZED VIEW regions_regions_mview AS (
    SELECT * FROM regions WHERE category = 'MAAKUNTA'
)
```

## Examples
```sql
-- Listing indicators
SELECT * FROM indicators;

-- Listing values for indicator 1111 for years 2013, 2018 and 2019
SELECT * FROM khrdata WHERE year in (2013, 2018, 2019) AND category = 'MAAKUNTA' and indicator = 1111;

-- Creating view for indicator 1111 for year 2013 and MAAKUNTA regions with geometries
CREATE VIEW total_area_conveyances_w_plan_mv  AS (
SELECT b.id, b.code, b.geom, a.indicator, a.year, a.value   FROM khrdata a INNER JOIN regions_regions_mview b ON a.region = b.id
WHERE year in (2013) AND a.category = 'MAAKUNTA' and indicator = 1111);


```



# License 
NLS khr data is licenced under [CC By 4.0](https://creativecommons.org/licenses/by/4.0/deed.en) license.
