import json
from logging import INFO
from typing import Iterable

import requests
from plpygis import Geometry

from khrfdw.base import GeoFDW

KHR_API_URL = "https://khr.maanmittauslaitos.fi/tilastopalvelu/rest/1.1/{}"
REGIONS = "regions"
GRID30X30 = "grid30x30"
GRID10X10 = "grid10x10"
GRID5X5 = "grid5x5"
REGION = "region"
SUBREGION = "subregion"
MUNICIPALITY = "municipality"
DATA = "data"
INDICATORS = "indicators"

DEFAULT_INDICATOR = 1111
DEFAULT_YEARS = [2020]

VALID_URLS = {
    # groups: "groups"
    INDICATORS: "indicators",
    DATA: 'categories/{category}/indicators/{indicator}/data?years={years}',
    REGIONS: 'categories/{category}/regions/geometry?crs=EPSG:{crs}'
}

VALID_CATEGORY_TYPES = {
    MUNICIPALITY: "KUNTA",
    SUBREGION: "SEUTUKUNTA",
    REGION: "MAAKUNTA",
    GRID5X5: "HILA5X5",
    GRID10X10: "HILA10X10",
    GRID30X30: "HILA30X30"
}


class Khr(GeoFDW):
    """A Foreign data wrapper for accessing KHR Rest API.
    """

    def __init__(self, options, columns):
        super(Khr, self).__init__(options, columns)
        self.key = self.get_option("url")
        self.category_type = self.get_option("category", required=False, default=VALID_CATEGORY_TYPES[REGION])
        self.srid = self.get_option("srid", required=False, default=3067,
                                    option_type=int)
        try:
            self.url = VALID_URLS.get(self.key)
        except ValueError as e:
            self.log("Invalid url value {}".format(options.get("url", "")))
            raise e

    def execute(self, quals, columns):
        if self.key == INDICATORS:
            data = self.get_indicators(quals, columns)
            for item in data:
                ret = {'id': item['id'], 'title_fi': item['title']['fi'],
                       'title_en': item['title']['en']}
                yield ret
        elif self.key == DATA:
            data = self.get_data(quals, columns)
            for item in data:
                ret = {'indicator': item['indicator'], 'region': item['region'], 'year': item['year'],
                       'value': item['value'], 'category': self.category_type}
                yield ret
        elif self.key == REGIONS:
            data = self.get_regions(quals, columns)
            if len(data):
                features = data["features"]
                for feature in features:
                    geom = Geometry.from_geojson(feature["geometry"], srid=self.srid)
                    props = feature["properties"]
                    row = {"geom": geom.wkb, "id": props["id"], "code": props["code"], "category": props["category"]}
                    yield row

    def get_regions(self, quals, columns):
        quals_dict = {qual.field_name: qual for qual in quals}
        category = quals_dict["category"].value if "category" in quals_dict else self.category_type
        return self.fetch(KHR_API_URL.format(self.url.format(category=category, crs=self.srid)))

    def get_indicators(self, quals, columns):
        url = KHR_API_URL.format(self.url)
        return self.fetch(url)

    def get_data(self, quals, columns):
        quals_dict = {qual.field_name: qual for qual in quals}
        if "year" in quals_dict.keys():
            qual = quals_dict["year"]
            years = qual.value
            if not isinstance(years, Iterable):
                years = [years]
        else:
            years = DEFAULT_YEARS

        indicator = quals_dict["indicator"].value if "indicator" in quals_dict else DEFAULT_INDICATOR
        if isinstance(indicator, Iterable):
            indicator = ",".join(map(str, map(int, indicator)))
        else:
            indicator = int(indicator)

        category = quals_dict["category"].value if "category" in quals_dict else self.category_type
        if "category" in quals_dict and quals_dict["category"].is_list_operator:
            category = list(category)[0]
        self.category_type = category

        url = self.url.format(category=category, indicator=indicator, years="&years=".join(map(str, map(int, years))))
        return self.fetch(KHR_API_URL.format(url))

    def fetch(self, url):
        self.log("URL is: {}".format(url), INFO)
        try:
            response = requests.get(url)
        except requests.exceptions.ConnectionError as e:
            self.log("Khr FDW: unable to connect to {}".format(url))
            return []
        except requests.exceptions.Timeout as e:
            self.log("Khr FDW: timeout connecting to {}".format(url))
            return []

        if response.ok:
            try:
                return json.loads(response.content)
            except ValueError as e:
                self.log("Khr FDW: invalid JSON")
                return []
        else:
            self.log("Khr FDW: server returned status code {} with text {} for url {}".format(response.status_code, response.text, url))
            return []
