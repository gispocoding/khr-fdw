#!/usr/bin/env python
from setuptools import setup
from khrfdw._version import __version__

setup(
    name="khrfdw",
    version=__version__,
    url="https://gitlab.com/gispocoding/khr-fdw",
    license="GNU GPLv3",
    author="Joona Laine",
    tests_require=["pytest"],
    author_email="joona@gispo.fi",
    description="Foreign Data Wrappers for NLS Finland KHR data",
    long_description="Foreign Data Wrappers for NLS Finland KHR data",
    packages=["khrfdw"],
    include_package_data=True,
    platforms="any",
    classifiers = [
    ],
    install_requires = [
        "multicorn>=1.3.4.dev0",
        "requests>=2.22.0",
        "plpygis>=0.1.0"
    ],
    keywords='gis geographical postgis fdw khr postgresql'
)